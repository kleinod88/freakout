/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef __BUTTON__
#define __BUTTON__


#include <Widget.h>
#include <TextureManager.h>
#include <string>


class Button : public Widget
{
private:
    std::string _Lable;
    std::string _ThemeName;
    bool _Selected;
    SDL_FRect *_Rect;
    SDL_FRect _LRect;

public:
    Button(Widget *parent, std::string buttonText, std::string themeName = "BUTTON");

    void ChangeText (std::string text, SDL_Color color = SDL_Color{0,0,0,255});
    void Render () override;
    void Update () override;

    void ChangeRect(SDL_FRect *rect) override;

    bool IsSelected () override
    {
        return _Selected;
    }

    void ChangeSelect() override
    {
        if (_Selected)
            _Selected = false;
        else
            _Selected = true;
    }
};

#endif // __BUTTON__
