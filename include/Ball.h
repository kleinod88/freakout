/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#ifndef __BALL__
#define __BALL__

#include <SDL.h>

class Ball
{
private:
    Ball();
    static Ball* _Instance;

    SDL_FRect *_Rect;
    bool _IsRunning;
    SDL_FRect _Speed;

public:

    static Ball* Inst()
    {
        if(_Instance==nullptr)
            _Instance = new Ball ();
        return _Instance;
    }

    void SetSize (float size);
    void SetPos(float x, float y);


    void Start();//lässt den Ball los fliegen
    void Reset();//der Ball wird wieder auf den Spieler zurück gesetzt

    bool IsRunning()
    {
        return _IsRunning;
    }

    void SetSpeed(float speed);

    SDL_FRect* GetRect()
    {
        return _Rect;
    }

    void Update ();
    void Draw();

    void ChangeSize();

    //Testet ob der ball mit einem anderen Rect zusammen stößt
    bool CollisionBlock (SDL_FRect *rect);
};
#endif //__BALL__
