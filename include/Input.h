/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef __INPUT__
#define __INPUT__

#include <vector>
#include <utility>
#include <SDL.h>
#include "Vector2D.h"


enum Mouse_Buttons
{
    LEFT = 0,
    MIDDLE = 1,
    RIGHT =2
};

class InputHandler
{
public:

    static InputHandler* Instance();

    bool Update();
    void Clean();


    void InitialiseJoysticks();
    bool JoysticksInitialised();


    int Axis(SDL_GameControllerAxis axis);
    
    bool LeftShoulder();
    bool RightShoulder();
    bool Up();
    bool Down();
    bool Left();
    bool Right();
    bool Select();
    bool Start();
    bool A();
    bool B();
    bool Y();
    bool X();
    bool LeftStick();
    bool RightStick();


    bool GetMouseButtonState(int buttonNumber);
    Vector2D* GetMousePosition();

    bool IsKeyDown(SDL_Scancode key);
private:
    InputHandler();
    ~InputHandler();

    static InputHandler* _Instance;

    //std::vector<SDL_GameController*> _Controller;
    SDL_GameController* _Controller;
    bool _JoysticksInitialised;

    std::vector<bool> _MouseButtonStates;
    Vector2D *_MousePosition;

    const Uint8* _Keystates;


    void _OnKeyDown();
    void _OnKeyUP();

    void _OnMouseMove(SDL_Event &event);
    void _OnMouseButtonDown(SDL_Event &event);
    void _OnMouseButtonUp(SDL_Event &event);

};

typedef InputHandler TheInputHandler;

#endif // INPUTHANDLER_H
