/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#ifndef __GAMESTATE__
#define __GAMESTATE__


class GameState
{
public:
    virtual bool Update() = 0;
    virtual void Render() = 0;
    virtual void OnEnter() = 0;
    virtual void OnExit() = 0;
//    virtual void ChanegSize(int,int) = 0;

    virtual ~GameState(){}
};//GameState

#endif //__GAMESTATE__
