/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#ifndef __STATEMANAGER__
#define __STATEMANAGER__

#include "GameState.h"

//GameStates
typedef enum GameStates{
    SPLASHSTATE,      //Bildschirm, der beim starten angezeigt wird
    MAINMENUSTATE,    //Hauptmenu
    PLAYSTATE,        //Das eigentliche Spiel
    PAUSESTATE,       //Pause-Bildschirm
    SCORESTATE,      //Namen Eingeben, wenn man einen neuen Record aufgestellt hat
    GAMEOVERSTATE,    //Game Over....du warst nicht gut genug
    MAXSTATES         //Der letzte state, nie in verwendung (hoffe ich :) )
}GameStates;

class StateManager
{
private:
    static StateManager* _Instance;

    int _CurrentState;

    GameState* _States[MAXSTATES];

    StateManager();
public:

    static StateManager* Inst();

    /**
     * @Brief Lad einen State
     *
     * @param state, der State der geladen werden soll
     * @param renderer, der SDL_Renderer, der für die Darstellung benötigt wird
     *
     * @todo wenn es eine Texturemanager-Klasse gibt, kann der letzte parameter entfallen
     *
     * @return true wenn der State erstellt wurde, false, wenn der State schon geladen wurde
     */
    bool AddState(GameStates state);

    /**
     * @brief Wechselt den Aktuellen Status des Spiels
     *
     * @param state der State zu dem gewechselt werden soll
     *
     * @return false, falls der angegebene State nicht geladen wurde
     */
    bool ChangeState(GameStates state);

    /**
     * @brief Löscht den angebenen State und gibt den Speicher frei
     *
     * @param state Der State, der gelöscht werden soll
     *
     * @return false, falls der State nicht geladen ist
     */
    bool RemoveState(GameStates state);

    /**
     * @brief Gibt den derzeitigen Spielstatus zurück
     *
     * @return Den Aktuellen Spielstatus oder nullptr, falls nichts geladen ist
     */
    GameState* CurrentState();


}; //StateManager

#endif //__STATEMANAGER__
