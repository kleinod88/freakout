/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef __THEME__
#define __THEME__

#include <string>
#include <SDL.h>
#include <CFGFile.h>
#include <fstream>
#include <TextureManager.h>

enum {LIGHTGREEN, LIGHTGREEN2, LIGHTBLUE, LIGHTBLUE2, RED, RED2, BROWN, BROWN2, DARKBLUE, DARKBLUE2, DARKGREEN, DARKGREEN2, GREY, GREY2, ORANGE, ORANGE2, PURPLE, PURPLE2, YELLOW, YELLOW2, COLORMAX};


class Theme {
private:
    std::string _BackgroundIMG;
    std::string _BallIMG;
    std::string _PlayerIMG;
    std::string _SplashIMG;
    std::string _ButtonIMG;
    std::string _ExtraSmallIMG;
    std::string _ExtraBigIMG;
    std::string _ExtraSlowIMG;
    std::string _ExtraFastIMG;
    std::string _BlockIMG[COLORMAX];

    SDL_Color _BackgroundColor; //Farbe mit der der Renderer gecleart wird

    int _PlayStateThemed; //wenn 0, dann gibt es kein Hintergrundbild beim spielen

    Theme(){
        LoadThemeFile();
    }

    bool LoadThemeFile (){
        std::ifstream ThemeFile(Config::Inst()->GetTheme().c_str());

        if (!ThemeFile.is_open())
        {
            SDL_Log ("Konnte Theme-Datei (%s) nicht laden!\n", Config::Inst()->GetTheme().c_str());
            return false;
        }

        char r, g, b, a, t;
        ThemeFile >> r >> g >> b >> t;
//        ThemeFile >> _PlayStateThemed;
        _BackgroundColor.a = 255;
        _BackgroundColor.r = r - 48;
        _BackgroundColor.g = g - 48;
        _BackgroundColor.b = b - 48;
        _PlayStateThemed = t - 48;

         SDL_Log("R: %d G: %d B: %d A: %d\nThemed: %d\n", _BackgroundColor.r, _BackgroundColor.g, _BackgroundColor.b, _BackgroundColor.a, _PlayStateThemed);
        SDL_ClearError();
        ThemeFile >> _BackgroundIMG;
        ThemeFile >> _BallIMG;
        ThemeFile >> _PlayerIMG;
        ThemeFile >> _SplashIMG;
        ThemeFile >> _ButtonIMG;
        ThemeFile >> _ExtraSmallIMG;
        ThemeFile >> _ExtraBigIMG;
        ThemeFile >> _ExtraSlowIMG;
        ThemeFile >> _ExtraFastIMG;
        ThemeFile >> _BlockIMG[LIGHTGREEN];
        ThemeFile >> _BlockIMG[LIGHTGREEN2];
        ThemeFile >> _BlockIMG[LIGHTBLUE];
        ThemeFile >> _BlockIMG[LIGHTBLUE2];
        ThemeFile >> _BlockIMG[RED];
        ThemeFile >> _BlockIMG[RED2];
        ThemeFile >> _BlockIMG[BROWN];
        ThemeFile >> _BlockIMG[BROWN2];
        ThemeFile >> _BlockIMG[DARKBLUE];
        ThemeFile >> _BlockIMG[DARKBLUE2];
        ThemeFile >> _BlockIMG[DARKGREEN];
        ThemeFile >> _BlockIMG[DARKGREEN2];
        ThemeFile >> _BlockIMG[GREY];
        ThemeFile >> _BlockIMG[GREY2];
        ThemeFile >> _BlockIMG[ORANGE];
        ThemeFile >> _BlockIMG[ORANGE2];
        ThemeFile >> _BlockIMG[PURPLE];
        ThemeFile >> _BlockIMG[PURPLE2];
        ThemeFile >> _BlockIMG[YELLOW];
        ThemeFile >> _BlockIMG[YELLOW2];
        return true;
    }


    static Theme *_Theme;
public:
    static Theme* Inst(){
        if (_Theme == nullptr)
            _Theme = new Theme();
        return _Theme;
    }
    bool LoadTheme(){
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _BackgroundIMG, "BACKGROUND");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _BallIMG, "BALL");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _PlayerIMG, "PLAYER");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _SplashIMG, "SPLASH");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _ButtonIMG, "BUTTON");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _ButtonIMG, "BUTTON_ACTIVE");
         TextureManager::Inst()->SetColorMod("BUTTON_ACTIVE", 100, 100 ,200);;
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _ExtraSmallIMG, "EXTRASMALL");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _ExtraBigIMG, "EXTRABIG");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _ExtraSlowIMG, "EXTRASMALL");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _ExtraFastIMG, "EXTRAFAST");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _BlockIMG[LIGHTGREEN], "LIGHT_GREEN");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _BlockIMG[LIGHTGREEN2], "LIGHT_GREEN2");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _BlockIMG[LIGHTBLUE], "LIGHT_BLUE");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _BlockIMG[LIGHTBLUE2], "LIGHT_BLUE2");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _BlockIMG[RED], "RED");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _BlockIMG[RED2], "RED2");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _BlockIMG[BROWN], "BROWN");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _BlockIMG[BROWN2], "BROWN2");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _BlockIMG[DARKBLUE], "DARK_BLUE");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _BlockIMG[DARKBLUE2], "DARK_BLUE2");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _BlockIMG[DARKGREEN], "DARK_GREEN");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _BlockIMG[DARKGREEN2], "DARK_GREEN2");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _BlockIMG[GREY], "GREY");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _BlockIMG[GREY2], "GREY2");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _BlockIMG[ORANGE], "ORANGE");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _BlockIMG[ORANGE2], "ORANGE2");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _BlockIMG[PURPLE], "PURPLE");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _BlockIMG[PURPLE2], "PURPLE2");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _BlockIMG[YELLOW], "YELLOW");
        TextureManager::Inst()->Load(Config::Inst()->GetDataDir() + _BlockIMG[YELLOW2], "YELLOW2");
        return true;
    }

    bool isThemed(){
        if (_PlayStateThemed > 0)
            return true;
        return false;
    }
};

#endif //__THEME__
