/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#ifndef __PLAYUI__
#define __PLAYUI__

#include <SDL.h>
#include <SDL_ttf.h>


class PlayUI
{
public:
    static PlayUI* Inst()
    {
        if (_Instance == nullptr)
                _Instance = new PlayUI;
        return _Instance;
    }

    void Init(SDL_Renderer *renderer);

    ~PlayUI();

    void Draw ();

    void ChangeSize() {}


    SDL_FRect* GetPlayFrame()
    {
        return _PlayFrame;
    }

private:
    PlayUI(){}
    static PlayUI* _Instance;
    SDL_Renderer *_Renderer;

    SDL_Texture *_LableScore;
    SDL_Texture *_LableLife;
    SDL_Texture *_LableLevel;

    SDL_FRect *_ScoreFrame;

    SDL_FRect *_LifeFrame;

    SDL_FRect *_LevelFrame;

    SDL_FRect *_PlayFrame;

    TTF_Font *_Font;

};//PlayUI
#endif //__PLAYUI__
