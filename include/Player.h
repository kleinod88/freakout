/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#ifndef __PLAYER__
#define __PLAYER__

#include <SDL.h>

const int   EXTRA_PLAYER_BIG = 20;
const int   EXTRA_PLAYER_SMALL = 30;

class Player
{
public:
    static Player* Inst()
    {
        if (_Instance == nullptr)
            _Instance = new Player;
        return _Instance;
    }

    void Create();
    void Update();
    void Move(int move);
    void AddPoint(int points);
    void AddLife(int life);
    void RemoveLife();
    void LevelUp();

    int GetScore();
    int GetLife();
    int GetLevel();

    SDL_FRect* GetRect();
    //Wichtig hier wird die linke untere ecke angegeben!!
    void SetPos(float x, float y);


    void ExtraBig();
    void ExtraSmall();

    void ChangeSize();

    void Draw ();
private:
    static Player *_Instance;

    Player();
    ~Player();

    SDL_FRect *_Rect;

    Uint32 _ExtraTimeStart;
    bool _ExtraBig;
    bool _ExtraSmall;

    int _Score;
    int _Life;
    int _Level;
}; //Player

#endif //__PLAYER__
