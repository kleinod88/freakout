/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef __CFGFILE__
#define __CFGFILE__

#include <string>
#include <SDL2/SDL.h>

class Config {
private:

    //Fenster und Renderer
    int _WindowWidth; //Breite des Fensters
    int _WindowHight; //Höhe des Fensters
    int _FullScreen; //Vollbild oder nicht
    int _Renderer; //Welches Renderbackend soll genutzt werden

    //Aussehen und Level
    std::string _ThemeFile; //Datei, in der alle zu Ladende Bilder Stehen
    std::string _LevelFile; //Datei, in der alle Levels stehen
    std::string _FontFile;  //Zu nutzender Font

    //Spieler, Blöcke und Ball
    float _PlayerWidth; //Breite des Spielers, 0.0f bis 1.0f, ausgehend von der Spielfeldbreite
    float _PlayerHight; //Höhe des Spielers, 0.0f bis 1.0f, ausgehend von der Spielfeldhöhe
    float _PlayerSpeed; //Wie schnell der Spieler sich hin und her bewegt
    float _BlockWidth;  //Breite der Blöcke, 0.0f bis 1.0f, ausgehend von der Spielfeldbreite
    float _BlockHight;  //Höhe der Blöcke, 0.0f bis 1.0f, ausgehend von der Spielfeldhöhe
    float _BallSize;    //Ball Größe, ausgehend von der Spielfeldhöhe
    float _BallSpeed;   //Geschwindigkeit des Balls

    Config()
    {
        SDL_Log ("WARNUNG: ConfigFile ist noch nicht nicht fertig geschrieben\n");
    }

    static Config* _Config;
public:

    static Config* Inst()
    {
        if (_Config == nullptr)
            _Config = new Config;
        return _Config;
    }

    int GetScreenWidth()
    {
        return 1280;
    }
    int GetScreenHight()
    {
        return 720;
    }
    bool GetFullScreen()
    {
        return false;
    }
    int GetRenderer()
    {
        return 1;//2 für virtualbox und 1 für einen pysischen pc (linux und windows)
    }
    std::string GetTheme()
    {
        std::string s (SDL_GetPrefPath("", "Freakout"));
        s += "data/default.fthm";
        return s;
    }
    std::string GetLevel()
    {
        std::string s (SDL_GetPrefPath("", "Freakout"));
        s += "data/default.lvl";
        return s;
    }
    std::string GetFont()
    {
        std::string s (SDL_GetPrefPath("", "Freakout"));
        s += "data/saarland.ttf";
        return s;
    }
    std::string GetDataDir()
    {
        std::string s (SDL_GetPrefPath("", "Freakout"));
        s += "data/";
        return s;
    }

    float GetPlayerWidth()
    {
        return 0.2;
    }
    float GetPlayerHight()
    {
        return 0.025;
    }
    float GetPlayerSpeed()
    {
        return 20.0;
    }
    float GetBlockWidth()
    {
        return 0.05;
    }
    float GetBlockHight()
    {
        return 0.057;
    }
    float GetBallSize()
    {
        return 0.03;
    }
    float GetBallSpeed()
    {
        return 3.f;
    }

};
#endif //__CFGFILE__
