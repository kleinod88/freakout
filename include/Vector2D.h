/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef VECTOR2D_H
#define VECTOR2D_H

class Vector2D
{
public:
    Vector2D(float x, float y);

    float GetX();
    float GetY();

    void SetX(float x);
    void SetY(float y);

    float Length();

    Vector2D operator+(const Vector2D &v2) const;
    friend Vector2D& operator+= (Vector2D& v1, const Vector2D& v2);

    Vector2D operator* (float scaler);
    Vector2D& operator*= (float scaler);

    Vector2D operator- (const Vector2D& v2);
    friend Vector2D& operator-=(Vector2D &v1, const Vector2D &v2);

    Vector2D operator/ (float scaler);
    Vector2D& operator/= (float scaler);

    void Normalize();
private:

    float _X;
    float _Y;
};

#endif // VECTOR2D_H
