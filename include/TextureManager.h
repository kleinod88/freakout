/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#ifndef __TEXTUREMANAGER__
#define __TEXTUREMANAGER__

#include <SDL.h>
#include <string>
#include <map>

class TextureManager {
public:

    static TextureManager* Inst();

    void Init(SDL_Renderer *renderer);

    bool Load (std::string fileName, std::string id);
    bool Load (std::string id, SDL_Surface *src);
    void Draw (std::string id, int x, int y, int width, int height, SDL_RendererFlip flip = SDL_FLIP_NONE);
    void DrawFS (std::string id, SDL_RendererFlip flip = SDL_FLIP_NONE);
    void DrawFrame (std::string id, int x, int y, int width, int height, int currentFrame, int currentRow, SDL_RendererFlip flip = SDL_FLIP_NONE);

    void ForcePresent();
    void ClearFromTextureMap (std::string id);

    void SetColorMod (std::string id,  int r, int g, int b)
    {
        SDL_SetTextureColorMod(_TextureMap[id], r, g, b);
    }

    void GetColorMod(std::string id,Uint8 *r, Uint8 *g, Uint8 *b)
    {
        SDL_GetTextureColorMod(_TextureMap[id], r, g, b);
    }

    SDL_Renderer* GetRenderer();
private:

    std::map<std::string,SDL_Texture*> _TextureMap;

    static TextureManager *_Instance;

    SDL_Renderer *_Renderer;
    TextureManager();
    ~TextureManager();
};


#endif //__TEXTUREMANAGER__
