/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#ifndef __BLOCK__
#define __BLOCK__

#include <SDL.h>



class Block
{
public:
    Block(int color, int extra);

    bool Update();
    void Draw();

	void SetSize(float w, float h);
	void SetPos(float x, float y);

    void SetColor(int color)
    {
        _Color = color;
    }

    void ChangeSize() {}



private:
    SDL_FRect *_Rect;
    int _Life;
    int _Color;

    bool _ExtraSmall;
    bool _ExtraBig;
    bool _ExtraActivated;
};
#endif //__BLOCK__
