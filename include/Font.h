/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef __FONT__
#define __FONT__

#include <SDL.h>
#include <string>

class Font {
private:
    SDL_Texture  *_Glyphs[128];
    int _Width[128];
    int _Height;

public:

    Font( int size, SDL_Color color = {0,0,0,255});
    ~Font();

    void RenderGlyph (unsigned short int ch, int x, int y, int height = 0, int max_width=0);

    void Renderstring (std::string str, SDL_Rect *destRect, bool enableHight = false);

};





#endif //__FONT__
