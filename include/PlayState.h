/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#ifndef __PLAYSTATE__
#define __PLAYSTATE__

#include "GameState.h"
#include "PlayUI.h"
#include "Block.h"

//Maximal Gr��e der "Gegner in bl�cke
#define BLOCK_MAX_W  20
#define BLOCK_MAX_H  6
#define MAX_LEVEL 10
#define PLAYER_STEP_WIDTH  20
#define BALL_FIXED_SPEED  5

class PlayState : public GameState
{
private:
    Block *_Blocks[MAX_LEVEL][BLOCK_MAX_W][BLOCK_MAX_H];


    int _Levels;
    bool _Win;

public:
    PlayState();
    bool Update () override;
    void Render () override;
    void OnEnter() override;
    void OnExit() override;
//    void ChanegSize(int, int) {}
}; //PlayState

#endif //__PLAYSTATE__
