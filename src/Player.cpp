/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include <Player.h>
#include <PlayUI.h>
#include <Ball.h>
#include <Game.h>
#include <TextureManager.h>
#include <CFGFile.h>

Player* Player::_Instance = nullptr;

Player::Player()
{
    _Rect = new SDL_FRect;

    _Score = 0;
    _Level = 1;
    _Life = 3;
}

Player::~Player()
{
}

void Player::Create()
{
    //TODO Dynamische größe
    //X und Y werden gesetzt, wenn das Spielfeld erzeugt wird!
    _Rect->w = PlayUI::Inst()->GetPlayFrame()->w * Config::Inst()->GetPlayerWidth();
    _Rect->h = PlayUI::Inst()->GetPlayFrame()->h * Config::Inst()->GetPlayerHight();
}

void Player::AddLife(int life)
{
    _Life += life;
}

void Player::AddPoint(int points)
{
    _Score += points;
}

int Player::GetLevel()
{
    return _Level;
}

int Player::GetLife()
{
    return _Life;
}

int Player::GetScore()
{
    return _Score;
}

void Player::SetPos(float x, float y)
{
    _Rect->x = x;
    _Rect->y = y;// - _Rect->h - 1;
    if (_Rect->x-1 < PlayUI::Inst()->GetPlayFrame()->x)
        _Rect->x = PlayUI::Inst()->GetPlayFrame()->x +1;
    if ((_Rect->x + _Rect->w + 1) >= (PlayUI::Inst()->GetPlayFrame()->x + PlayUI::Inst()->GetPlayFrame()->w))
        _Rect->x = (PlayUI::Inst()->GetPlayFrame()->x + PlayUI::Inst()->GetPlayFrame()->w) - _Rect->w - 1;
}

SDL_FRect * Player::GetRect()
{
    return _Rect;
}

void Player::RemoveLife()
{
    _Life--;
}

void Player::Move(int move)
{
    //TODO nicht über das Spielfeld
    _Rect->x += move;
    if (_Rect->x  < PlayUI::Inst()->GetPlayFrame()->x)
        _Rect->x = PlayUI::Inst()->GetPlayFrame()->x +1;
    if ((_Rect->x + _Rect->w) > (PlayUI::Inst()->GetPlayFrame()->x + PlayUI::Inst()->GetPlayFrame()->w))
        _Rect->x = (PlayUI::Inst()->GetPlayFrame()->x + PlayUI::Inst()->GetPlayFrame()->w) - _Rect->w - 1;
    if (!Ball::Inst()->IsRunning())
    {
        Ball::Inst()->Reset();
    }
}


void Player::Draw()
{
     TextureManager::Inst()->Draw("PLAYER", _Rect->x, _Rect->y, _Rect->w, _Rect->h);
}


void Player::ChangeSize()
{
    _Rect->w = PlayUI::Inst()->GetPlayFrame()->w * Config::Inst()->GetPlayerWidth();
    _Rect->h =PlayUI::Inst()->GetPlayFrame()->h * Config::Inst()->GetPlayerHight();
}

void Player::LevelUp()
{
    TextureManager::Inst()->DrawFS("LEVELUP");
    TextureManager::Inst()->ForcePresent();
    SDL_Delay(600);
    _Level++;
    _Life++;
    Ball::Inst()->Reset();
}

void Player::ExtraBig() {
    _ExtraTimeStart = SDL_GetTicks();
    _Rect->w += 30;
    _ExtraBig = true;
}
void Player::ExtraSmall() {
    _ExtraTimeStart = SDL_GetTicks();
    _Rect->w -= 30;
    _ExtraSmall = true;
}

void Player::Update() {
    if (_ExtraSmall == true) {
        if (_ExtraTimeStart + 5000 < SDL_GetTicks())
        {
            _Rect->w += 30;
            _ExtraSmall = false;
        }
    }
    if (_ExtraBig == true) {
        if (_ExtraTimeStart + 5000 < SDL_GetTicks())
        {
            _Rect->w -= 30;
            _ExtraBig = false;
        }
    }
}
