/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include <Block.h>
#include <Ball.h>
#include <Player.h>
#include <PlayUI.h>
#include <TextureManager.h>
#include <CFGFile.h>


bool IsLoad = false;


Block::Block(int color, int extra){
    _Rect = new SDL_FRect;
    _Rect->x = 0;
    _Rect->y = 0;
    _Rect->w = PlayUI::Inst()->GetPlayFrame()->w * Config::Inst()->GetBlockWidth();
    _Rect->h = PlayUI::Inst()->GetPlayFrame()->h * Config::Inst()->GetBlockHight();

    _Life = 2;
    _Color = color;
    _ExtraActivated = false;
    _ExtraBig = false;
    _ExtraSmall = false;
    if (extra == 1)
        _ExtraBig = true;
    if (extra == 2)
        _ExtraSmall = true;


}

void Block::Draw(){
    switch (_Color)
    {
    case 1:
        if (_Life == 2)
            TextureManager::Inst()->Draw("RED", _Rect->x, _Rect->y, _Rect->w, _Rect->h);
        else if (_Life == 1)
            TextureManager::Inst()->Draw("RED2", _Rect->x, _Rect->y, _Rect->w, _Rect->h);
        break;
    case 2:
        if (_Life == 2)
            TextureManager::Inst()->Draw("YELLOW", _Rect->x, _Rect->y, _Rect->w, _Rect->h);
        else if (_Life == 1)
            TextureManager::Inst()->Draw("YELLOW2", _Rect->x, _Rect->y, _Rect->w, _Rect->h);
        break;
    case 3:
        if (_Life == 2)
            TextureManager::Inst()->Draw("DARK_BLUE", _Rect->x, _Rect->y, _Rect->w, _Rect->h);
        else if (_Life == 1)
            TextureManager::Inst()->Draw("DARK_BLUE2", _Rect->x, _Rect->y, _Rect->w, _Rect->h);
        break;
    case 4:
        if (_Life == 2)
            TextureManager::Inst()->Draw("LIGHT_GREEN", _Rect->x, _Rect->y, _Rect->w, _Rect->h);
        else if (_Life == 1)
            TextureManager::Inst()->Draw("LIGHT_GREEN2", _Rect->x, _Rect->y, _Rect->w, _Rect->h);
        break;
    case 5:
        if (_Life == 2)
            TextureManager::Inst()->Draw("BROWN", _Rect->x, _Rect->y, _Rect->w, _Rect->h);
        else if (_Life == 1)
            TextureManager::Inst()->Draw("BROWN2", _Rect->x, _Rect->y, _Rect->w, _Rect->h);
        break;
    case 6:
        if (_Life == 2)
            TextureManager::Inst()->Draw("PURPLE", _Rect->x, _Rect->y, _Rect->w, _Rect->h);
        else if (_Life == 1)
            TextureManager::Inst()->Draw("PURPLE2", _Rect->x, _Rect->y, _Rect->w, _Rect->h);
        break;
    case 7:
        if (_Life == 2)
            TextureManager::Inst()->Draw("GREY", _Rect->x, _Rect->y, _Rect->w, _Rect->h);
        else if (_Life == 1)
            TextureManager::Inst()->Draw("GREY2", _Rect->x, _Rect->y, _Rect->w, _Rect->h);
        break;
    case 8:
        if (_Life == 2)
            TextureManager::Inst()->Draw("ORANGE", _Rect->x, _Rect->y, _Rect->w, _Rect->h);
        else if (_Life == 1)
            TextureManager::Inst()->Draw("ORANGE2", _Rect->x, _Rect->y, _Rect->w, _Rect->h);
        break;
    case 9:
        if (_Life == 2)
            TextureManager::Inst()->Draw("DARK_GREEN", _Rect->x, _Rect->y, _Rect->w, _Rect->h);
        else if (_Life == 1)
            TextureManager::Inst()->Draw("DARK_GREEN2", _Rect->x, _Rect->y, _Rect->w, _Rect->h);
        break;
    case 10:
        if (_Life == 2)
            TextureManager::Inst()->Draw("LIGHT_BLUE", _Rect->x, _Rect->y, _Rect->w, _Rect->h);
        else if (_Life == 1)
            TextureManager::Inst()->Draw("LIGHT_BLUE2", _Rect->x, _Rect->y, _Rect->w, _Rect->h);
        break;

    default:
        break;
    }

    if (_ExtraActivated)
    {
        if (_ExtraBig)
            TextureManager::Inst()->Draw("EXTRABIG", _Rect->x, _Rect->y, _Rect->w, _Rect->h);
        if (_ExtraSmall)
            TextureManager::Inst()->Draw("EXTRASMALL", _Rect->x, _Rect->y, _Rect->w, _Rect->h);
    }
}


//TODO, Punkte eventuell von schwierigkeitsgrad abh�nging machen?
bool Block::Update(){
    if (_Life > 0 && _Color > 0){
        if (Ball::Inst()->CollisionBlock(_Rect)){
            if (_Life > 0){
                _Life--;
                Player::Inst()->AddPoint(20);
                if (_Life == 0) {
                    _ExtraActivated = true;
                    _Rect->w *= 2;
                }
            }
        }
        return true;
    }
    if (_ExtraActivated)
    {
        _Rect->y += 3;
        SDL_Rect r, r2;
        r.x = _Rect->x;
        r.y = _Rect->y;
        r.h = _Rect->h;
        r.w = _Rect->w;
        r2.x = Player::Inst()->GetRect()->x;
        r2.y = Player::Inst()->GetRect()->y;
        r2.h = Player::Inst()->GetRect()->h;
        r2.w = Player::Inst()->GetRect()->w;

        if (SDL_HasIntersection(&r, &r2)) {
            if (_ExtraBig)
                Player::Inst()->ExtraBig();
            if (_ExtraSmall)
                Player::Inst()->ExtraSmall();
            _ExtraActivated = false;
        }
        if (_Rect->y + _Rect->h >= PlayUI::Inst()->GetPlayFrame()->y + PlayUI::Inst()->GetPlayFrame()->h) {
            _ExtraActivated = false;
        }
    }
    return false;
}

void Block::SetPos(float x, float y){
    _Rect->x = x+1;
    _Rect->y = y+1;
}

void Block::SetSize(float w, float h){
    _Rect->w = PlayUI::Inst()->GetPlayFrame()->w * Config::Inst()->GetBlockWidth();
    _Rect->h = PlayUI::Inst()->GetPlayFrame()->h * Config::Inst()->GetBlockHight();
}

