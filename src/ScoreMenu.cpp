/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include <ScoreMenu.h>
#include <Widget.h>
#include <Input.h>
#include <TextureManager.h>
#include <StateManager.h>
#include <GUIText.h>
#include <Lable.h>



ScoreMenu::ScoreMenu()
{
}

void ScoreMenu::OnExit()
{
}

void ScoreMenu::Render()
{
    //Box->Render();
    int w, h;
    SDL_GetRendererOutputSize(TextureManager::Inst()->GetRenderer(), &w, &h);
    //rect1 = new SDL_FRect;
    rect1.x = 100;
    rect1.y = 0;
    rect1.w = w - 200;
    rect1.h = h;

    SDL_Rect _Header;
    SDL_Rect _Hight1;
    SDL_Rect _Hight2;
    SDL_Rect _Hight3;
    SDL_Rect _Hight4;
    SDL_Rect _Hight5;
    SDL_Rect _Hight6;
    SDL_Rect _Hight7;
    SDL_Rect _Hight8;
    SDL_Rect _Hight9;
    SDL_Rect _Hight10;

    SDL_Rect _Name1;
    SDL_Rect _Name2;
    SDL_Rect _Name3;
    SDL_Rect _Name4;
    SDL_Rect _Name5;
    SDL_Rect _Name6;
    SDL_Rect _Name7;
    SDL_Rect _Name8;
    SDL_Rect _Name9;
    SDL_Rect _Name10;

    SDL_Rect _Score1;
    SDL_Rect _Score2;
    SDL_Rect _Score3;
    SDL_Rect _Score4;
    SDL_Rect _Score5;
    SDL_Rect _Score6;
    SDL_Rect _Score7;
    SDL_Rect _Score8;
    SDL_Rect _Score9;
    SDL_Rect _Score10;

    _Header. x = 0;
    _Header.y = 0;
    _Header.w = w;
    _Header.h = (h *20 )/100;


    _Hight1.x = 0;
    _Hight2.x = 0;
    _Hight3.x = 0;
    _Hight4.x = 0;
    _Hight5.x = 0;
    _Hight6.x = 0;
    _Hight7.x = 0;
    _Hight8.x = 0;
    _Hight9.x = 0;
    _Hight10.x = 0;

    int tmph = h - _Header.h;
    tmph = tmph / 10;
    SDL_Log("HeaderH: %d       tmph: %d\n", _Header.h, tmph);
    _Hight1.h = _Hight2.h = _Hight3.h = _Hight4.h = _Hight5.h = _Hight6.h = _Hight7.h = _Hight8.h = _Hight9.h = _Hight10.h = tmph;
    _Hight1.w = _Hight2.w = _Hight3.w = _Hight4.w = _Hight5.w = _Hight6.w = _Hight7.w = _Hight8.w = _Hight9.w = _Hight10.w = 120;
    _Hight1.y = _Header.h;
    _Hight2.y = _Hight1.y + tmph;
    _Hight3.y = _Hight2.y + tmph;
    _Hight4.y = _Hight3.y + tmph;
    _Hight5.y = _Hight4.y + tmph;
    _Hight6.y = _Hight5.y + tmph;
    _Hight7.y = _Hight6.y + tmph;
    _Hight8.y = _Hight7.y + tmph;
    _Hight9.y = _Hight8.y + tmph;
    _Hight10.y = _Hight9.y + tmph;

     _Name1.x = 120;
    _Name2.x = 120;
    _Name3.x = 120;
    _Name4.x = 120;
    _Name5.x = 120;
    _Name6.x = 120;
    _Name7.x = 120;
    _Name8.x = 120;
    _Name9.x = 120;
    _Name10.x = 120;

    int hw = (w - 120)/2;

     _Score1.x = hw+120;
     _Score2.x = hw+120;
     _Score3.x = hw+120;
     _Score4.x = hw+120;
     _Score5.x = hw+120;
     _Score6.x = hw+120;
     _Score7.x = hw+120;
     _Score8.x = hw+120;
     _Score9.x = hw+120;
     _Score10.x = hw+120;


     _Name1.w = _Score1.w = hw;
    _Name2.w = _Score2.w = hw;
    _Name3.w = _Score3.w = hw;
    _Name4.w = _Score4.w = hw;
    _Name5.w = _Score5.w = hw;
    _Name6.w = _Score6.w = hw;
    _Name7.w = _Score7.w = hw;
    _Name8.w = _Score8.w = hw;
    _Name9.w = _Score9.w = hw;
    _Name10.w = _Score10.w = hw;

    _Name1.y = _Score1.y = _Hight1.y;
    _Name2.y = _Score2.y = _Hight2.y;
    _Name3.y = _Score3.y = _Hight3.y;
    _Name4.y = _Score4.y = _Hight4.y;
    _Name5.y = _Score5.y = _Hight5.y;
    _Name6.y = _Score6.y = _Hight6.y;
    _Name7.y = _Score7.y = _Hight7.y;
    _Name8.y = _Score8.y = _Hight8.y;
    _Name9.y = _Score9.y = _Hight9.y;
    _Name10.y = _Score10.y = _Hight10.y;
   _Name1.h = _Name2.h = _Name3.h = _Name4.h = _Name5.h = _Name6.h = _Name7.h = _Name8.h = _Name9.h = _Name10.h = tmph;
   _Score1.h = _Score2.h = _Score3.h = _Score4.h = _Score5.h = _Score6.h = _Score7.h = _Score8.h = _Score9.h = _Score10.h = tmph;


    GFont->Renderstring("Bestenliste", &_Header, true);
    GFont->Renderstring(" 1.:", &_Hight1, true);
    GFont->Renderstring("Kleinod", &_Name1, true);
    GFont->Renderstring("999999", &_Score1, true);
    GFont->Renderstring(" 2.:", &_Hight2, true);
    GFont->Renderstring("Mullemaus", &_Name2, true);
    GFont->Renderstring("888888", &_Score2, true);
    GFont->Renderstring(" 3.:", &_Hight3, true);
    GFont->Renderstring("Thoren", &_Name3, true);
    GFont->Renderstring("777777", &_Score3, true);
    GFont->Renderstring(" 4.:", &_Hight4, true);
    GFont->Renderstring("Emma", &_Name4, true);
    GFont->Renderstring("666666", &_Score4, true);
    GFont->Renderstring(" 5.:", &_Hight5, true);
    GFont->Renderstring("Kilian", &_Name5, true);
    GFont->Renderstring("555555", &_Score5, true);
    GFont->Renderstring(" 6.:", &_Hight6, true);
    GFont->Renderstring("Rita", &_Name6, true);
    GFont->Renderstring("444444", &_Score6, true);
    GFont->Renderstring(" 7.:", &_Hight7, true);
    GFont->Renderstring("Wolfgang", &_Name7, true);
    GFont->Renderstring("333333", &_Score7, true);
    GFont->Renderstring(" 8.:", &_Hight8, true);
    GFont->Renderstring("Paul", &_Name8, true);
    GFont->Renderstring("222222", &_Score8, true);
    GFont->Renderstring(" 9.:", &_Hight9, true);
    GFont->Renderstring("Ramona", &_Name9, true);
    GFont->Renderstring("111111", &_Score9, true);
    GFont->Renderstring("10.:", &_Hight10, true);
    GFont->Renderstring("Niemand", &_Name10, true);
    GFont->Renderstring("______", &_Score10, true);

}

bool ScoreMenu::Update()
{
    if (InputHandler::Instance()->A())
        StateManager::Inst()->ChangeState(MAINMENUSTATE);
    return true;
}

void ScoreMenu::OnEnter()
{
    int w, h;
    SDL_GetRendererOutputSize(TextureManager::Inst()->GetRenderer(), &w, &h);
    //rect1 = new SDL_FRect;
    rect1.x = 100;
    rect1.y = 0;
    rect1.w = w - 200;
    rect1.h = h;

    SDL_Rect _Header;
    _Header. x = 0;
    _Header.y = 0;
    _Header.w = w;
    _Header.h = h;
    GFont->Renderstring("Bestenliste", &_Header);
    
/*
    Box = new HBox(nullptr, &rect1);
    vBox[0] = new VBox(Box, nullptr);
    vBox[1] = new VBox(Box, nullptr);
    vBox[2] = new VBox(Box, nullptr);
    vBox[3] = new VBox(Box, nullptr);
    vBox[4] = new VBox(Box, nullptr);
    vBox[5] = new VBox(Box, nullptr);
    vBox[6] = new VBox(Box, nullptr);
    vBox[7] = new VBox(Box, nullptr);
    vBox[8] = new VBox(Box, nullptr);
    vBox[9] = new VBox(Box, nullptr);
    Box->AddChild(new Lable(Box, " Bestenliste "));
    Box->AddChild(vBox[0]);
    Box->AddChild(vBox[1]);
    Box->AddChild(vBox[2]);
    Box->AddChild(vBox[3]);
    Box->AddChild(vBox[4]);
    Box->AddChild(vBox[5]);
    Box->AddChild(vBox[6]);
    Box->AddChild(vBox[7]);
    Box->AddChild(vBox[8]);
    Box->AddChild(vBox[9]);
    vBox[0]->AddChild(new Lable(vBox[0], "                   1.: "));
    vBox[0]->AddChild(new Lable(vBox[0], " Frank"));
    vBox[0]->AddChild(new Lable(vBox[0], "999"));
    vBox[1]->AddChild(new Lable(vBox[1], "                    2.: "));
    vBox[1]->AddChild(new Lable(vBox[1], " Katrin"));
    vBox[1]->AddChild(new Lable(vBox[1], " 888"));
    vBox[2]->AddChild(new Lable(vBox[2], "                    3.: "));
    vBox[2]->AddChild(new Lable(vBox[2], " Thoren                 "));
    vBox[2]->AddChild(new Lable(vBox[2], "              777"));
    vBox[3]->AddChild(new Lable(vBox[3], "                    4.: "));
    vBox[3]->AddChild(new Lable(vBox[3], " Emma                   "));
    vBox[3]->AddChild(new Lable(vBox[3], "              666"));
    vBox[4]->AddChild(new Lable(vBox[4], "                    5.: "));
    vBox[4]->AddChild(new Lable(vBox[4], " Kilian                 "));
    vBox[4]->AddChild(new Lable(vBox[4], "              555"));
    vBox[5]->AddChild(new Lable(vBox[5], "                    6.: "));
    vBox[5]->AddChild(new Lable(vBox[5], " Wolfgang               "));
    vBox[5]->AddChild(new Lable(vBox[5], "              444"));
    vBox[6]->AddChild(new Lable(vBox[6], "                    7.: "));
    vBox[6]->AddChild(new Lable(vBox[6], " Rita "));
    vBox[6]->AddChild(new Lable(vBox[6], " 333"));
    vBox[7]->AddChild(new Lable(vBox[7], "                    8.: "));
    vBox[7]->AddChild(new Lable(vBox[7], " Ramona                 "));
    vBox[7]->AddChild(new Lable(vBox[7], "              222"));
    vBox[8]->AddChild(new Lable(vBox[8], "                    9.: "));
    vBox[8]->AddChild(new Lable(vBox[8], " Paul                   "));
    vBox[8]->AddChild(new Lable(vBox[8], "              111"));
    vBox[9]->AddChild(new Lable(vBox[9], "                   10.: "));
    vBox[9]->AddChild(new Lable(vBox[9], " Unbekannt              "));
    vBox[9]->AddChild(new Lable(vBox[9], "              000"));*/
}


