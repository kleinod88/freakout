/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <HBox.h>
#include <Input.h>

//TODO widgets müssen ihre größe preisgeben bzw ihr rect
HBox::HBox(Widget *parent, SDL_FRect *rect)
{
    _Parent = parent;
    _Size = new SDL_FRect;

    if (rect != nullptr)
    {
        _Size->x = rect->x;
        _Size->y = rect->y;
        _Size->h = rect->h;
        _Size->w = rect->w;
    }
    else
    {
        _Size->x = 0;
        _Size->y = 0;
        _Size->w = 1;
        _Size->h = 1;
    }
    _ChildCount = 0;
    _AktivChild = 0;
}

void HBox::AddChild(Widget* newChild)
{
    if (_ChildCount >= 29)
        return;

    _Childs[_ChildCount] = newChild;
    _CRect[_ChildCount] = new SDL_FRect;
    _ChildCount++;

    for (int i = 0; i < _ChildCount; i++)
    {
        _CRect[i]->x = _Size->x;
        _CRect[i]->w = _Size->w;
        _CRect[i]->h = _Size->h / _ChildCount;
        _CRect[i]->y = i * _CRect[i]->h;

        _Childs[i]->ChangeRect(_CRect[i]);
    }

    if (_AktivChild == 0)
    {
        _AktivChild = 1;
        _Childs[0]->ChangeSelect();
    }
}

void HBox::ChangeRect(SDL_FRect* rect)
{
    _Size->x = rect->x;
    _Size->y = rect->y;
    _Size->h = rect->h;
    _Size->w = rect->w;

    for (int i = 0; i < _ChildCount; i++)
    {
        _CRect[i]->x = _Size->x;
        _CRect[i]->w = _Size->w;
        _CRect[i]->h = _Size->h / _ChildCount;
        _CRect[i]->y = i * _CRect[i]->h;

        _Childs[i]->ChangeRect(_CRect[i]);
    }
}

void HBox::Render()
{
    for (int i = 0; i < _ChildCount; i++)
    {
        _Childs[i]->Render();
    }
}

void HBox::Update()
{
    if (_ChildCount == 0)
        return;

    if (InputHandler::Instance()->Down())
    {
        if (_AktivChild  < _ChildCount)
        {
            _Childs[_AktivChild-1]->ChangeSelect();
            _Childs[_AktivChild]->ChangeSelect();
            _AktivChild++;
            SDL_Delay(90);
        }
        else
        {
            _Childs[_AktivChild-1]->ChangeSelect();
            _Childs[0]->ChangeSelect();
            _AktivChild = 1;
            SDL_Delay(90);
        }
    }
    else if (InputHandler::Instance()->Up())
    {
         SDL_Log ("Change is _AktivChild[%d] \n", _AktivChild);
        if (_AktivChild  > 1)
        {
            _Childs[_AktivChild-1]->ChangeSelect();
            _Childs[_AktivChild-2]->ChangeSelect();
            SDL_Log ("Change1 from _AktivChild[%d] zu _AktivChild[%d]\n", _AktivChild, _AktivChild-1);
            _AktivChild--;
            SDL_Delay(90);
        }
        else
        {
            _Childs[_AktivChild-1]->ChangeSelect();
            _Childs[_ChildCount-1]->ChangeSelect();
            SDL_Log ("Change2 from _AktivChild[%d] zu _AktivChild[%d]\n", _AktivChild, _ChildCount);
            _AktivChild = _ChildCount;
            SDL_Delay(90);
        }
    }
    if (_ChildCount > 0)
    {
        _Childs[_AktivChild - 1]->Update();
    }
}
