/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <GUIText.h>
#include <CFGFile.h>


GUIText::GUIText(std::string id)
{
    _ID = id;
    _Font = nullptr;
    _Font = TTF_OpenFont (Config::Inst()->GetFont().c_str(), 100);

    if (_Font == nullptr)
    {
        SDL_Log("Konnte den Font nicht öffnen! %s\n", SDL_GetError());
    }
    SetText("_LABLE_", SDL_Color{ 0,0,0,255 });
}

std::string GUIText::GetText()
{
    return _TXT;
}

void GUIText::Render ( SDL_FRect* rect )
{
    TextureManager::Inst()->Draw(_ID, (int)rect->x, (int)rect->y, (int)rect->w, (int)rect->h);
}

void GUIText::SetText ( std::string text, SDL_Color color )
{
    _TXT = text;

    //TextureManager::Inst()->ClearFromTextureMap(_ID);
    
    SDL_Surface *surf = TTF_RenderUTF8_Blended(_Font, _TXT.c_str(), color);
    if (surf == nullptr)
        SDL_Log("Konnte Text nicht rendern: %s\n", SDL_GetError());
    TextureManager::Inst()->Load(_ID, surf);
}
