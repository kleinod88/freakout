/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include <PauseState.h>
#include <StateManager.h>
#include <TextureManager.h>
#include <Input.h>
#include <Button.h>
#include <Lable.h>


class PlayButton : public Button {
public:
    PlayButton(Widget* parent, std::string buttonText, std::string themeName = "BUTTON")
        :Button(parent, buttonText, themeName)
    {}
    void Update() override
    {
        if (InputHandler::Instance()->A())
            StateManager::Inst()->ChangeState(PLAYSTATE);
    }
};


class ExitButton : public Button {
public:
    ExitButton(Widget* parent, std::string buttonText, std::string themeName = "BUTTON")
        :Button(parent, buttonText, themeName)
    {}
    void Update() override
    {///muss noch besser werden
        if (InputHandler::Instance()->A())
            SDL_Quit();
    }
};

PauseState::PauseState()
{

}

void PauseState::OnExit()
{
}

void PauseState::Render()
{
       // TextureManager::Inst()->DrawFS("PAUS");
    Box->Render();
}

bool PauseState::Update()
{
    Box->Update();


    
    return true;
}

void PauseState::OnEnter()
{
    SDL_FRect rect = { 200, 100, 300, 500 };
    Box = new HBox(nullptr, &rect);

    Box->AddChild(new Lable(Box, "      Pause    "));
    Box->AddChild(new PlayButton(Box, "Weiter"));
    Box->AddChild(new ExitButton(Box, "Beenden"));

}

