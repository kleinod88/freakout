/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include <Ball.h>
#include <PlayUI.h>
#include <Player.h>
#include <TextureManager.h>
#include <CFGFile.h>

Ball* Ball::_Instance = nullptr;

Ball::Ball (){
    _Rect = new SDL_FRect;
    _Rect->x = 0;
    _Rect->y = 0;
    _Rect->w = _Rect->h = PlayUI::Inst()->GetPlayFrame()->h * Config::Inst()->GetBallSize();
    _IsRunning = false;

    _Speed.x = _Speed.y = Config::Inst()->GetBallSpeed();
}


void Ball::Update(){
    int left, right, top, bottun;

    left = PlayUI::Inst()->GetPlayFrame()->x;
    right = PlayUI::Inst()->GetPlayFrame()->x + PlayUI::Inst()->GetPlayFrame()->w;
    top = PlayUI::Inst()->GetPlayFrame()->y;
    bottun = PlayUI::Inst()->GetPlayFrame()->y + PlayUI::Inst()->GetPlayFrame()->h;
    //wenn linken oder rechten rand Berührt, speed.x = -speed.x;
    if (_Rect->x <= left || _Rect->x + _Rect->w >= right){
        _Speed.x = -_Speed.x;
    }
    //Wenn oben berührt speed.y = -speed.y
    if (_Rect->y <= top){
        _Speed.y = -_Speed.y;
    }
    //Wenn boden berührt PLayer->Life -1  und reset
    if (_Rect->y + _Rect->h >= bottun){
        Player::Inst()->RemoveLife();
        Reset();
    }

    //Wenn spieler berührt player->point + 10 && speed.y = -speed.y
    SDL_Rect r, r2;
    r.x = _Rect->x;
    r.y = _Rect->y;
    r.h = _Rect->h;
    r.w = _Rect->w;
    r2.x = Player::Inst()->GetRect()->x;
    r2.y = Player::Inst()->GetRect()->y;
    r2.h = Player::Inst()->GetRect()->h;
    r2.w = Player::Inst()->GetRect()->w;

    if (SDL_HasIntersection(&r, &r2)){
        Player::Inst()->AddPoint(10);
        if ( _Speed.y < 0 )
            ;
        else
            _Speed.y = -_Speed.y;
    }

    _Rect->y += _Speed.y;
    _Rect->x += _Speed.x;
}

void Ball::Draw(){
    TextureManager::Inst()->Draw("BALL", _Rect->x, _Rect->y, _Rect->w, _Rect->h);
}

void Ball::Reset(){
    _IsRunning = false;
    SetPos(Player::Inst()->GetRect()->x + ((Player::Inst()->GetRect()->w /2) - (_Rect->w / 2)), Player::Inst()->GetRect()->y  - _Rect->h);
}


void Ball::SetPos(float x, float y){
    _Rect->x = x;
    _Rect->y = y;
}

void Ball::SetSize(float size){
    _Rect->w = _Rect->h = PlayUI::Inst()->GetPlayFrame()->h * Config::Inst()->GetBallSize();
}

void Ball::SetSpeed(float speed){
    _Speed.x = speed;
    _Speed.y = speed;
}


void Ball::Start(){
    if (IsRunning())
        return;
    _Speed.y = -_Speed.y;

    //Wenn ball links von spielfeldmitte -> x = -x

    int tmpX;
    tmpX = PlayUI::Inst()->GetPlayFrame()->w / 2;
    tmpX += PlayUI::Inst()->GetPlayFrame()->x;

    if (_Rect->x < tmpX){
        _Speed.x = -_Speed.x;
    }

    _IsRunning = true;
}

bool Ball::CollisionBlock(SDL_FRect* rect){
    bool rvalue = false;
    if (((_Rect->x > rect->x && _Rect->x < rect->x + rect->w) ||
         (_Rect->x + _Rect->w > rect->x && _Rect->x + _Rect->w < rect->x + rect->w)) &&
        ((_Rect->y < rect->y + rect->h && _Rect->y > rect->y  + rect->h - (rect->w / 3)) ||
         (_Rect->y + _Rect->h  > rect->y && _Rect->y + _Rect->h < rect->y + (rect->w / 3))))
    {
        _Speed.y = -_Speed.y;
        _Rect->y += _Speed.y;
        rvalue = true;
    }

    if (((_Rect->y > rect->y && _Rect->y < rect->y + rect->h) ||
         (_Rect->y + _Rect->h > rect->y && _Rect->y + _Rect->h < rect->y + rect->h)) &&
        ((_Rect->x < rect->x + rect->w && _Rect->x > rect->x  + rect->w - (rect->h / 3)) ||
         (_Rect->x + _Rect->w  > rect->x && _Rect->x + _Rect->w < rect->x + (rect->h / 3))))
    {
        _Speed.x = -_Speed.x;
        _Rect->x += _Speed.x;
        rvalue = true;
    }

    //sind wir in der breite des Blocks unterwegs
    return rvalue;
}

void Ball::ChangeSize(){
    _Rect->w = _Rect->h = PlayUI::Inst()->GetPlayFrame()->h * Config::Inst()->GetBallSize();
}
