/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include <Input.h>

#include <Game.h>

InputHandler* InputHandler::_Instance = nullptr;

const int _JoystickDeadZone = 10000;

InputHandler* InputHandler::Instance()
{
    if (_Instance == nullptr)
        _Instance = new InputHandler;
    return _Instance;
}

InputHandler::InputHandler()
{
    for(int i = 0; i < 3; i++)
    {
        _MouseButtonStates.push_back(false);
    }
    _MousePosition = new Vector2D(0,0);
    _Keystates = SDL_GetKeyboardState(0);
    _JoysticksInitialised = false;
}

InputHandler::~InputHandler()
{
}

void InputHandler::Clean()
{
    
}

bool InputHandler::Update()
{
    SDL_Event event;
    _Keystates = SDL_GetKeyboardState(0);
    while (SDL_PollEvent(&event))
    {
        switch (event.type)
        {

        case SDL_QUIT:
            return false;
            break;
        case SDL_MOUSEBUTTONDOWN:
            _OnMouseButtonDown(event);
            break;
        case SDL_MOUSEBUTTONUP:
            _OnMouseButtonUp(event);
            break;
        case SDL_MOUSEMOTION:
            _OnMouseMove(event);
            break;
        case SDL_KEYDOWN:
            _OnKeyDown();
            break;
        case SDL_KEYUP:
            _OnKeyUP();
            break;
        default:
            break;
        }
    }
    return true;
}

void InputHandler::_OnKeyDown()
{
    _Keystates = SDL_GetKeyboardState(0);
}

void InputHandler::_OnKeyUP()
{
    _Keystates = SDL_GetKeyboardState(0);
}

void InputHandler::_OnMouseButtonUp(SDL_Event& event)
{
    if(event.button.button == SDL_BUTTON_LEFT)
        _MouseButtonStates[LEFT] = false;
    if(event.button.button == SDL_BUTTON_MIDDLE)
        _MouseButtonStates[MIDDLE] = false;
    if(event.button.button == SDL_BUTTON_RIGHT)
        _MouseButtonStates[RIGHT] = false;
}

void InputHandler::_OnMouseButtonDown(SDL_Event& event)
{
    if(event.button.button == SDL_BUTTON_LEFT)
        _MouseButtonStates[LEFT] = true;
    if(event.button.button == SDL_BUTTON_MIDDLE)
        _MouseButtonStates[MIDDLE] = true;
    if(event.button.button == SDL_BUTTON_RIGHT)
        _MouseButtonStates[RIGHT] = true;
}

void InputHandler::_OnMouseMove(SDL_Event& event)
{
    _MousePosition->SetX(event.motion.x);
    _MousePosition->SetY(event.motion.y);
}





void InputHandler::InitialiseJoysticks()
{
    if (SDL_WasInit(SDL_INIT_JOYSTICK)==0)
    {
        SDL_InitSubSystem(SDL_INIT_JOYSTICK);
    }

    if (SDL_NumJoysticks()>0)
    {
        for (int i = 0; i < SDL_NumJoysticks(); i++)
        {
            SDL_Joystick* joy = SDL_JoystickOpen(i);
            if (joy)
            {
                if (SDL_IsGameController(i))
                {
                    _Controller = SDL_GameControllerOpen(i);
                }
            }

        }
        SDL_JoystickEventState(SDL_ENABLE);
        _JoysticksInitialised = true;
    }
    else
    {
        SDL_LogWarn (SDL_LOG_CATEGORY_CUSTOM, "Konnte keine Controller finden\n");
    }

}


bool InputHandler::JoysticksInitialised()
{
    return _JoysticksInitialised;
}


bool InputHandler::GetMouseButtonState(int buttonNumber)
{
    return _MouseButtonStates[buttonNumber];
}

Vector2D * InputHandler::GetMousePosition()
{
    return _MousePosition;
}

bool InputHandler::IsKeyDown(SDL_Scancode key)
{
    if (_Keystates != nullptr)
    {
        if (_Keystates[key] == 1)
            return true;
    }
    return false;
}

//FIXME zur zeit immer der erste controller
int InputHandler::Axis(SDL_GameControllerAxis axis)
{
    if (_JoysticksInitialised)
        return SDL_GameControllerGetAxis(_Controller, axis);
    else
        return 0;
}

bool InputHandler::LeftShoulder()
{
    if (_JoysticksInitialised)
    {
        if (SDL_GameControllerGetButton(_Controller, SDL_CONTROLLER_BUTTON_LEFTSHOULDER) || IsKeyDown(SDL_SCANCODE_LEFT))
            return true;
        else
            return false;
    }
    else
    {
        if ( IsKeyDown(SDL_SCANCODE_LEFT))
            return true;
        else
            return false;
    }
}
bool InputHandler::RightShoulder()
{
    if (_JoysticksInitialised)
    {
        if (SDL_GameControllerGetButton(_Controller, SDL_CONTROLLER_BUTTON_RIGHTSHOULDER) || IsKeyDown(SDL_SCANCODE_RIGHT))
            return true;
        else
            return false;
    }
    else
    {
        if (IsKeyDown(SDL_SCANCODE_RIGHT))
            return true;
        else
            return false;
    }
}
bool InputHandler::Up()
{
    if (_JoysticksInitialised)
    {
        if (SDL_GameControllerGetButton(_Controller, SDL_CONTROLLER_BUTTON_DPAD_UP) || IsKeyDown(SDL_SCANCODE_UP))
            return true;
        else
            return false;
    }
    else
    {
        if (IsKeyDown(SDL_SCANCODE_UP))
            return true;
        else
            return false;
    }
}
bool InputHandler::Down()
{
    if (_JoysticksInitialised)
    {
        if (SDL_GameControllerGetButton(_Controller, SDL_CONTROLLER_BUTTON_DPAD_DOWN) || IsKeyDown(SDL_SCANCODE_DOWN))
            return true;
        else
            return false;
    }
    else
    {
    if (IsKeyDown(SDL_SCANCODE_DOWN))
        return true;
    else
        return false;
    }
}
bool InputHandler::Left()
{
    if (_JoysticksInitialised)
    {
        if (SDL_GameControllerGetButton(_Controller, SDL_CONTROLLER_BUTTON_DPAD_LEFT) || IsKeyDown(SDL_SCANCODE_LEFT))
            return true;
        else
            return false;
    }
    else
    {
        if (IsKeyDown(SDL_SCANCODE_LEFT))
            return true;
        else
            return false;
    }
}
bool InputHandler::Right()
{
    if (_JoysticksInitialised)
    {
        if (SDL_GameControllerGetButton(_Controller, SDL_CONTROLLER_BUTTON_DPAD_RIGHT) || IsKeyDown(SDL_SCANCODE_RIGHT))
            return true;
        else
            return false;
    }
    else
    {
        if (IsKeyDown(SDL_SCANCODE_RIGHT))
            return true;
        else
            return false;
    }
}
bool InputHandler::Select()
{
    if (_JoysticksInitialised)
    {
        if (SDL_GameControllerGetButton(_Controller, SDL_CONTROLLER_BUTTON_LEFTSHOULDER) || IsKeyDown(SDL_SCANCODE_S))
            return true;
        else
            return false;
    }
    else
    {
        if (IsKeyDown(SDL_SCANCODE_S))
            return true;
        else
            return false;
    }
}
bool InputHandler::Start()
{
    if (_JoysticksInitialised)
    {
        if (SDL_GameControllerGetButton(_Controller, SDL_CONTROLLER_BUTTON_START) || IsKeyDown(SDL_SCANCODE_ESCAPE))
            return true;
        else
            return false;
    }
    else
    {
        if (IsKeyDown(SDL_SCANCODE_ESCAPE))
            return true;
        else
            return false;
    }
}
bool InputHandler::A()
{
    if (_JoysticksInitialised)
    {
        if (SDL_GameControllerGetButton(_Controller, SDL_CONTROLLER_BUTTON_A) || IsKeyDown(SDL_SCANCODE_A))
            return true;
        else
            return false;
    }
    else
    {
        if (IsKeyDown(SDL_SCANCODE_A))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
bool InputHandler::B()
{
    if (_JoysticksInitialised)
    {
        if (SDL_GameControllerGetButton(_Controller, SDL_CONTROLLER_BUTTON_B) || IsKeyDown(SDL_SCANCODE_B))
            return true;
        else
            return false;
    }
    else
    {
        if (IsKeyDown(SDL_SCANCODE_B))
            return true;
        else
            return false;
    }
}
bool InputHandler::Y()
{
    if (_JoysticksInitialised)
    {
        if (SDL_GameControllerGetButton(_Controller, SDL_CONTROLLER_BUTTON_Y) || IsKeyDown(SDL_SCANCODE_Y))
            return true;
        else
            return false;
    }
    else
    {
        if (IsKeyDown(SDL_SCANCODE_Y))
            return true;
        else
            return false;
    }
}
bool InputHandler::X()
{
    if (_JoysticksInitialised)
    {
        if (SDL_GameControllerGetButton(_Controller, SDL_CONTROLLER_BUTTON_X) || IsKeyDown(SDL_SCANCODE_X))
            return true;
        else
            return false;
    }
    else
    {
        if (IsKeyDown(SDL_SCANCODE_X))
            return true;
        else
            return false;
    }
}
bool InputHandler::LeftStick()
{
    if (_JoysticksInitialised)
    {
        if (SDL_GameControllerGetButton(_Controller, SDL_CONTROLLER_BUTTON_LEFTSTICK) || IsKeyDown(SDL_SCANCODE_Q))
            return true;
        else
            return false;
    }
    else
    {
        if (IsKeyDown(SDL_SCANCODE_Q))
            return true;
        else
            return false;
    }
}
bool InputHandler::RightStick()
{
    if (_JoysticksInitialised)
    {
        if (SDL_GameControllerGetButton(_Controller, SDL_CONTROLLER_BUTTON_RIGHTSTICK) || IsKeyDown(SDL_SCANCODE_E))
            return true;
        else
            return false;
    }
    else
    {
        if (IsKeyDown(SDL_SCANCODE_E))
            return true;
        else
            return false;
    }
}