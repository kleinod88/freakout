/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include <PlayUI.h>
#include <Player.h>
#include <string>
#include <Game.h>

PlayUI* PlayUI::_Instance = nullptr;



void PlayUI::Init(SDL_Renderer *renderer)
{
    _Renderer = renderer;

    int x = 0;
    int w = 0;
    int h = 0;

    SDL_GetRendererOutputSize(_Renderer, &w, &h);

    _PlayFrame = new SDL_FRect;
    _PlayFrame->x = w/80;
    _PlayFrame->y = w/80;
    _PlayFrame->w = 80 * (w / 110);
    _PlayFrame->h = h - (_PlayFrame->x * 2);

    int offset = _PlayFrame->w +_PlayFrame->x*2;

    _ScoreFrame = new SDL_FRect;
    x = offset;
    _ScoreFrame->x = (x/100) + _PlayFrame->x + _PlayFrame->w ;
    _ScoreFrame->y = _PlayFrame->y * 2;
    _ScoreFrame->w = w - offset;
    _ScoreFrame->h = 100;
    

    _LevelFrame = new SDL_FRect;
    x = offset;
    _LevelFrame->x = (x/100) + _PlayFrame->x + _PlayFrame->w ;
    _LevelFrame->y = _PlayFrame->y * 2 + _ScoreFrame->y + 150;
    _LevelFrame->w = w - offset;
    _LevelFrame->h = 100;
    
    _LifeFrame = new SDL_FRect;
    x = offset;
    _LifeFrame->x = (x/100) + _PlayFrame->x + _PlayFrame->w ;
    _LifeFrame->y = _PlayFrame->y * 2 + _ScoreFrame->y + 300 + 100;
    _LifeFrame->w = w - offset;
    _LifeFrame->h = 100;
}



void PlayUI::Draw()
{
    //Texturen erstellen
    std::string lableScore("Punkte:");

    lableScore += std::to_string(Player::Inst()->GetScore());

    std::string lableLevel("Level:");

    lableLevel += std::to_string(Player::Inst()->GetLevel());

    std::string lableLife("Leben:");

    lableLife += std::to_string(Player::Inst()->GetLife());



    SDL_SetRenderDrawColor(_Renderer,255,255,255,105);
    SDL_RenderFillRectF(_Renderer, _PlayFrame);
    SDL_SetRenderDrawColor(_Renderer,255,255,255,55);
    SDL_RenderFillRectF(_Renderer, _ScoreFrame);
    SDL_RenderFillRectF(_Renderer, _LevelFrame);
    SDL_RenderFillRectF(_Renderer, _LifeFrame);
    SDL_SetRenderDrawColor(_Renderer, 255, 255, 255, 255);
    SDL_Rect r1, r2, r3;

    r1.x =  _ScoreFrame->x;
    r1.y = _ScoreFrame->y;
    r1.h = _ScoreFrame->h;
    r1.w = _ScoreFrame->w;
    r2.x =_LevelFrame->x;
    r2.y =_LevelFrame->y;
    r2.h =_LevelFrame->h;
    r2.w =_LevelFrame->w;
    r3.x =_LifeFrame->x;
    r3.y =_LifeFrame->y;
    r3.h =_LifeFrame->h;
    r3.w =_LifeFrame->w;


    GFont->Renderstring(lableScore, &r1 );
    GFont->Renderstring(lableLevel, &r2 );
    GFont->Renderstring(lableLife, &r3 );
//     SDL_RenderCopyF(_Renderer, _LableScore, nullptr, _ScoreFrame);
//     SDL_RenderCopyF(_Renderer, _LableLevel, nullptr, _LevelFrame);
//     SDL_RenderCopyF(_Renderer, _LableLife, nullptr, _LifeFrame);

}


PlayUI::~PlayUI()
{
}

