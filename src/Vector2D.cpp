/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include "Vector2D.h"
#include <math.h>

Vector2D::Vector2D (float x, float y)
{
    _X = x;
    _Y = y;
}

float Vector2D::GetX()
{
    return _X;
}

float Vector2D::GetY()
{
    return _Y;
}


void Vector2D::SetX(float x)
{
    _X = x;
}

void Vector2D::SetY(float y)
{
    _Y = y;
}

float Vector2D::Length()
{
    return sqrt(_X*_X + _Y * _Y);
}

Vector2D Vector2D::operator+(const Vector2D& v2) const
{
    return Vector2D(_X+v2._X, _Y + v2._Y);
}

Vector2D& operator+=(Vector2D& v1,const Vector2D& v2)
{
    v1._X += v2._X;
    v1._Y += v2._Y;

    return v1;
}

Vector2D Vector2D::operator*(float scaler)
{
    return Vector2D(_X*scaler, _Y*scaler);
}
Vector2D & Vector2D::operator*=(float scaler)
{
    _X *= scaler;
    _Y *= scaler;

    return *this;
}

Vector2D Vector2D::operator-(const Vector2D& v2)
{
    return Vector2D(_X-v2._X, _Y-v2._Y);
}

Vector2D& operator-=(Vector2D& v1, const Vector2D& v2)
{
    v1._X -= v2._X;
    v1._Y -= v2._Y;

    return v1;
}

Vector2D Vector2D::operator/(float scaler)
{
    return Vector2D(_X/scaler,_Y/scaler);
}

Vector2D & Vector2D::operator/=(float scaler)
{
    _X /= scaler;
    _Y /= scaler;

    return *this;
}

void Vector2D::Normalize()
{
    float l = Length();
    if (l>0)
    {
        (*this) *= 1/1;
    }
}
