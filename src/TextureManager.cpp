/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include "TextureManager.h"
#include <SDL_image.h>
#include <iostream>

TextureManager* TextureManager::_Instance = nullptr;

TextureManager::TextureManager()
{
}

TextureManager::~TextureManager()
{
}

void TextureManager::Init(SDL_Renderer *renderer)
{
    _Renderer = renderer;
}

TextureManager* TextureManager::Inst()
{
    if (_Instance == nullptr)
        _Instance = new TextureManager;
    return _Instance;
}


bool TextureManager::Load(std::string fileName, std::string id)
{
    SDL_Surface *tmpSurf = IMG_Load(fileName.c_str());
    if (tmpSurf == nullptr)
    {
        SDL_Log ("Konnte Bild \"%s\" nicht Laden: %s\n", fileName.c_str(), SDL_GetError());
        return false;
    }
    else
    {
        SDL_Texture *tmpTex = SDL_CreateTextureFromSurface(_Renderer, tmpSurf);
        if (tmpTex == nullptr)
        {
            SDL_Log ("Konnte Texture von Bild \"%s\" nicht erstellen: %s\n", fileName.c_str(), SDL_GetError());
            SDL_FreeSurface(tmpSurf);
            return false;
        }
        else
        {
            SDL_FreeSurface(tmpSurf);
            _TextureMap[id] = tmpTex;
        }
    }
    SDL_Log("%s als %s geladen: %s\n", fileName.c_str(), id.c_str(), SDL_GetError());
    return true;
}

bool TextureManager::Load ( std::string id, SDL_Surface* src )
{
    SDL_Texture* tmpTex = SDL_CreateTextureFromSurface(_Renderer, src);
    if (tmpTex == nullptr)
    {
        SDL_Log ("Konnte Texture \"%s\" nicht erstellen: %s\n", id.c_str(), SDL_GetError());
        return false;
    }
    else
    {
        SDL_Log("Texture ID (%s): %s", id.c_str(), SDL_GetError());
        _TextureMap[id] = tmpTex;
    }

    return true;
}



void TextureManager::Draw(std::string id, int x, int y, int width, int height, SDL_RendererFlip flip)
{
    SDL_Rect srcRect;
    SDL_Rect destRect;

    srcRect.x = 0;
    srcRect.y = 0;
    srcRect.h = destRect.h = height;
    srcRect.w = destRect.w = width;
    destRect.x = x;
    destRect.y = y;


    SDL_RenderCopyEx(_Renderer, _TextureMap[id],nullptr, &destRect, 0, 0, flip);
}

void TextureManager::DrawFS(std::string id, SDL_RendererFlip flip)
{

    SDL_RenderCopyEx(_Renderer, _TextureMap[id], nullptr, nullptr, 0, 0, flip);
}


void TextureManager::ForcePresent()
{
    SDL_RenderPresent(_Renderer);
}

SDL_Renderer* TextureManager::GetRenderer()
{
    return _Renderer;
}

void TextureManager::DrawFrame(std::string id, int x, int y, int width, int height, int currentFrame, int currentRow, SDL_RendererFlip flip)
{
    SDL_Rect srcRect;
    SDL_Rect destRect;

    srcRect.x = width * currentFrame;
    srcRect.y = height *(currentRow -1);
    srcRect.h = destRect.h = height;
    srcRect.w = destRect.w = width;
    destRect.x = x;
    destRect.y = y;


    SDL_RenderCopyEx(_Renderer, _TextureMap[id],&srcRect, &destRect, 0, 0, flip);
}

void TextureManager::ClearFromTextureMap(std::string id)
{
    _TextureMap.erase(id);
}

