/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include <Game.h>
#include <SDL_image.h>
#include <StateManager.h>
#include <TextureManager.h>
#include <Player.h>
#include <Input.h>
#include <CFGFile.h>
#include <Theme.h>

Font *GFont = nullptr;

Game::Game(){
    /**
    * SDL Initialisieren und Fenster Erstellen
    * 
    * TODO: Fenster gr��e und modus sowie Renderer aus Config laden
    */
    if ( SDL_Init (SDL_INIT_EVERYTHING) != 0 ){
        SDL_LogError (SDL_LOG_CATEGORY_CUSTOM, "Konnte SDL nicht initialisieren: %s\n", SDL_GetError());
    }
    _Window = SDL_CreateWindow ("Freakout", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, Config::Inst()->GetScreenWidth(), Config::Inst()->GetScreenHight(), SDL_WINDOW_SHOWN /*| SDL_WINDOW_FULLSCREEN_DESKTOP*/ );
    if ( _Window == nullptr ){
        SDL_LogError (SDL_LOG_CATEGORY_CUSTOM, "Konnte kein Fenster erstellen: %s\n", SDL_GetError());
    }
    //F�r virtualbox muss es renderer 2 sein, sonst l�uft es zu schnell
    _Renderer = SDL_CreateRenderer ( _Window, Config::Inst()->GetRenderer(), SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
    if ( _Renderer == nullptr ){
        SDL_LogError (SDL_LOG_CATEGORY_CUSTOM, "Konnte Renderer nicht erstellen: %s\n", SDL_GetError());
    }
     SDL_SetRenderDrawBlendMode(_Renderer,SDL_BLENDMODE_BLEND);
    
    if (IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_WEBP) != 0){
        SDL_LogWarn (SDL_LOG_CATEGORY_CUSTOM, "Fehler beim Initialisieren von SDL_Image: %s\n", SDL_GetError());
    }
    
    //TextureManager Initialisieren und splash laden, um den splashscreen zur verf�gung stellen zu k�nnen
    TextureManager::Inst()->Init(_Renderer);

    GFont = new Font (100,SDL_Color{255,0,0,255});

    SDL_ClearError();

    StateManager::Inst()->AddState(SPLASHSTATE);
    SDL_ClearError();
    StateManager::Inst()->AddState(PLAYSTATE);
    SDL_ClearError();
    StateManager::Inst()->AddState(GAMEOVERSTATE);
    SDL_ClearError();
    StateManager::Inst()->AddState(PAUSESTATE);
    SDL_ClearError();
    StateManager::Inst()->AddState(MAINMENUSTATE);
    SDL_ClearError();
    StateManager::Inst()->AddState(SCORESTATE);
    SDL_ClearError();

    //Bilder Laden
    
    Theme::Inst()->LoadTheme();
    TextureManager::Inst()->Load("data/gameover.png", "GAMEOVER");
    TextureManager::Inst()->Load("data/gameover2.png", "GAMEOVER2");
    TextureManager::Inst()->Load("data/levelup.png", "LEVELUP");
    TextureManager::Inst()->Load("data/pause.png", "PAUS");


    //JoyStick bzw Controller initialisieren, falls am System angeschlo�en
    InputHandler::Instance()->InitialiseJoysticks();

    //Der erste State ist nat�rlich das MainMenu
    StateManager::Inst()->ChangeState(MAINMENUSTATE);
//    StateManager::Inst()->ChangeState(PLAYSTATE);

    _IsPaus = false;
    _GameOver = false;
}

Game::~Game()
{
    if (_Renderer != nullptr)
        SDL_DestroyRenderer(_Renderer);
    if (_Window != nullptr)
        SDL_DestroyWindow(_Window);
    SDL_Quit();
}

void Game::Quit()
{
    _IsRunning = false;
}

void Game::Run()
{
    _IsRunning = true;
    while (_IsRunning)
    {
         //Gucken ob das Fenster geschlo�en werden soll
        if ( !InputHandler::Instance()->Update() )
        {
            _IsRunning = false;
            return;
        }


        StateManager::Inst()->CurrentState()->Update();

        SDL_SetRenderDrawColor(_Renderer,50, 55, 255, 255);
        SDL_RenderClear(_Renderer);

        StateManager::Inst()->CurrentState()->Render();

        if (Player::Inst()->GetLife() < 1)
        {
            if (!_GameOver)
            {
                StateManager::Inst()->ChangeState(GAMEOVERSTATE);
                _GameOver = true;
            }
        }
        SDL_RenderPresent(_Renderer);
    }

}
