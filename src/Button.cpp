/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <Button.h>
#include <Game.h>

Button::Button(Widget *parent, std::string buttonText, std::string themeName){
    _Lable = buttonText;
    _ThemeName = themeName;
    _Selected = false;
    _Rect = new SDL_FRect;
    _Rect->x = _Rect->y = 0;
    _Rect->h = _Rect->w = 1;
    _Parent = parent;
    _Childs[0] = nullptr;
}

void Button::Update(){
}

void Button::ChangeRect(SDL_FRect* rect){
    _Rect->x = rect->x;
    _Rect->y = rect->y;
    _Rect->h = rect->h;
    _Rect->w = rect->w;
    _LRect.x = _Rect->x + ((_Rect->w * 10) / 100);
    _LRect.y = _Rect->y + ((_Rect->h * 10) / 100);
    _LRect.w = _Rect->w - ((_LRect.x - _Rect->x) * 2);
    _LRect.h = _Rect->h - ((_LRect.y - _Rect->y) * 2);
}


void Button::ChangeText(std::string text, SDL_Color color){
    _Lable = text;
}

void Button::Render(){

    SDL_Rect  r;
    r.x = _LRect.x;
    r.y = _LRect.y;
    r.w = _LRect.w;
    r.h = _LRect.h;


    Uint8 rc, g, b;

    TextureManager::Inst()->GetColorMod(_ThemeName, &rc,&g,&b);;
    //FIXME, die Farbe um den aktiven Knopf an zu zeigen selbst wählen
    if (_Selected){
//         TextureManager::Inst()->SetColorMod(_ThemeName, rc,g+20,b);;
         TextureManager::Inst()->Draw("BUTTON_ACTIVE", _Rect->x, _Rect->y, _Rect->w, _Rect->h);
//         TextureManager::Inst()->SetColorMod(_ThemeName, rc+20, g, b);
    }
    else
        TextureManager::Inst()->Draw(_ThemeName, _Rect->x, _Rect->y, _Rect->w, _Rect->h);
    GFont->Renderstring(_Lable, &r, true );
}
