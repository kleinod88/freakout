/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <Font.h>
#include <SDL_ttf.h>
#include <TextureManager.h>
#include <CFGFile.h>

Font::Font( int size, SDL_Color color)
{
    TTF_Init();
    TTF_Font *font = TTF_OpenFont	(Config::Inst()->GetFont().c_str(), size);
    if (font == nullptr)
    {
        SDL_Log ("Konnte Font nicht öffnen: %s\n", SDL_GetError());
        _Height = 0;
        return;
    }
    SDL_Surface *tmpSurface = nullptr;
    _Height = TTF_FontHeight(font);
    for (int i = 32; i < 128; i++)
    {
        tmpSurface = TTF_RenderGlyph_Solid(font, i, color);
        if (tmpSurface == nullptr)
        {
            SDL_Log ("Konnte Buchstabe %c nicht Rendern\n", i);
        }
        else
        {
            _Glyphs[i] = SDL_CreateTextureFromSurface(TextureManager::Inst()->GetRenderer(), tmpSurface);
            _Width[i] = tmpSurface->w;
            SDL_FreeSurface(tmpSurface);
        }
    }
     TTF_CloseFont(font);
     TTF_Quit();
}

Font::~Font()
{
}

void Font::RenderGlyph (unsigned short int ch, int x, int y, int height, int max_width)
{
    SDL_Rect r;
    r.x = x;
    r.y = y;
    if (height>0)
        r.h = height;
    else
        r.h = _Height;
    r.w = _Width[ch];

    SDL_RenderCopy(TextureManager::Inst()->GetRenderer(), _Glyphs[ch], nullptr, &r);
}

void Font::Renderstring (std::string str, SDL_Rect *destRect, bool enableHight)
{
    int xLocal = destRect->x;

    int stringWidth  = 0;

    for (long i = 0; i < str.size(); i++)
    {
        stringWidth +=  _Width[str.c_str()[i]];
    }

        if (stringWidth < destRect->w)
        {
            int tmpX = destRect->w - stringWidth;
            tmpX = tmpX / 2;
            destRect->x += tmpX;
        }

        if (_Height < destRect->h)
        {
            int tmpY = destRect->h - _Height;
            tmpY = tmpY / 2;
            destRect->y += tmpY;
        }
    xLocal = destRect->x;

    //wir haben kein destRect
    for (long i = 0; i < str.size(); i++)
    {
        if (xLocal + _Width[str.c_str()[i]] > destRect->x + destRect->w)
            return;
        if (enableHight)
            RenderGlyph(str.c_str()[i], xLocal, destRect->y, destRect->h, destRect->w);
        else
            RenderGlyph(str.c_str()[i], xLocal, destRect->y, 0, 0);
        xLocal += _Width[str.c_str()[i]];
    }
}
