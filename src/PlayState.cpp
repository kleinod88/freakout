/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include <PlayState.h>
#include <Player.h>
#include <Ball.h>
#include <PlayUI.h>
#include <StateManager.h>
#include <SDL_image.h>
#include <iostream>
#include <fstream>
#include <TextureManager.h>
#include <Input.h>
#include <CFGFile.h>
#include <Theme.h>

int BlocksLife[MAX_LEVEL][BLOCK_MAX_W][BLOCK_MAX_H];

int LoadLevels()
{
    std::ifstream LevelFile(Config::Inst()->GetLevel());

    if (!LevelFile.is_open())
    {
        SDL_Log ("Konnte Level-Datei (%s) nicht laden!\n", Config::Inst()->GetLevel().c_str());
    }

    int LevelInFile = 0;
    
    LevelFile >> LevelInFile;
    
    for (int i = 0; i < LevelInFile; i++)
    {
        for (int b = 0; b < BLOCK_MAX_H; b++)
        {
            for (int a = 0; a < BLOCK_MAX_W; a++)
            {
                LevelFile >> BlocksLife[i][a][b];
            }
        }
    }
    return LevelInFile;
}

PlayState::PlayState()
{
    
}

void PlayState::OnExit()
{
}

void PlayState::Render()
{
    if (Theme::Inst()->isThemed())
        TextureManager::Inst()->DrawFS("BACKGROUND");
    else
    {
        SDL_SetRenderDrawColor(TextureManager::Inst()->GetRenderer(), 50, 55, 255, 255);
        SDL_RenderClear(TextureManager::Inst()->GetRenderer());
    }

    //Das Userinterface Rendern
    PlayUI::Inst()->Draw();

    //Den Spieler Rendern
    Player::Inst()->Draw();

    //Den Ball Renderern
    Ball::Inst()->Draw();

    for (int a = 0; a < BLOCK_MAX_W; a++)
    {
        for (int b = 0; b < BLOCK_MAX_H; b++)
        {
            _Blocks[Player::Inst()->GetLevel()-1][a][b]->Draw();;
        }
    }
}

bool PlayState::Update()
{
    //Spieler nach links bewegen
    if (InputHandler::Instance()->LeftShoulder())
    {
        Player::Inst()->Move(-Config::Inst()->GetPlayerSpeed());
    }
    //Spieler nach rechts bewegen
    if (InputHandler::Instance()->RightShoulder())
    {
        Player::Inst()->Move(Config::Inst()->GetPlayerSpeed());
    }
    if (InputHandler::Instance()->Start() )
    {
//        SDL_Delay(200);
        StateManager::Inst()->ChangeState(PAUSESTATE);
    }

    
    //Spiel Starten
    if (InputHandler::Instance()->A())
    {
        Ball::Inst()->Start();
    }

    //Wenn der Ball gestartet wurde, den Ball Updaten
    if (Ball::Inst()->IsRunning())
        Ball::Inst()->Update();

    //Die Bl�cke/Gegner Updaten
    int Win = 0;
    for (int a = 0; a < BLOCK_MAX_W; a++)
    {
        for (int b = 0; b < BLOCK_MAX_H; b++)
        {
            if (_Blocks[Player::Inst()->GetLevel()-1][a][b]->Update())
                Win++;
        }
    }
    if (Win == 0 && Player::Inst()->GetLevel() < _Levels && Ball::Inst()->IsRunning())
    {
        SDL_Log ("Player level %d\n",Player::Inst()->GetLevel()-1);
        Player::Inst()->LevelUp();
        _Win = false;
    }
    else if (Win == 0 )
    {
        _Win = true;
    }
    Player::Inst()->Update();
    return true;
}

void PlayState::OnEnter()
{
    _Levels = LoadLevels();
    //Userinterface erstellen
    PlayUI::Inst()->Init(TextureManager::Inst()->GetRenderer());

    //Spieler erstellen
    Player::Inst()->Create();
    Player::Inst()->SetPos(PlayUI::Inst()->GetPlayFrame()->x - (PlayUI::Inst()->GetPlayFrame()->w - ((Player::Inst()->GetRect()->w / 2)) / 2), PlayUI::Inst()->GetPlayFrame()->y + PlayUI::Inst()->GetPlayFrame()->h - Player::Inst()->GetRect()->h);


    //Den Ball vorbereiten
    /*
    * Der erste aufruf von einer ball instance, mit Renderer, siehe Ball.h

    */
    Ball::Inst()->ChangeSize();
//    Ball::Inst()->SetSpeed(BALL_FIXED_SPEED);
    Ball::Inst()->Reset();
    
    //TODO das muss noch irgendwo anders hin, damit es sich �ndern kann, wenn die Fenster fgr��e angepasst wird
    //TODO aus Textdatei laden
    //Position der Bl�cke im Spielfeld
    int left, top, h, w;
    left = PlayUI::Inst()->GetPlayFrame()->x;
    top = PlayUI::Inst()->GetPlayFrame()->y;
    w = PlayUI::Inst()->GetPlayFrame()->w / BLOCK_MAX_W;
    h = (PlayUI::Inst()->GetPlayFrame()->h / 3) / BLOCK_MAX_H;
    Player::Inst()->SetPos(PlayUI::Inst()->GetPlayFrame()->x - (PlayUI::Inst()->GetPlayFrame()->w - ((Player::Inst()->GetRect()->w / 2)) / 2), PlayUI::Inst()->GetPlayFrame()->y + PlayUI::Inst()->GetPlayFrame()->h - Player::Inst()->GetRect()->h);


   
    for (int i = 0; i < _Levels; i++)
    {
        for (int a = 0; a < 20; a++)
        {
            for (int b = 0; b < 6; b++)
            {
                if (BlocksLife[i][a][b] > 30)
                    _Blocks[i][a][b] = new Block(BlocksLife[i][a][b]-30, 2);
                else if (BlocksLife[i][a][b] > 20)
                    _Blocks[i][a][b] = new Block(BlocksLife[i][a][b]-20, 1);
                else
                    _Blocks[i][a][b] = new Block(BlocksLife[i][a][b], 0);
                _Blocks[i][a][b]->SetSize(w, h);
                int blockX = left + (w * a + 1);

                int blockY = top + (h * b + 1);
                _Blocks[i][a][b]->SetPos(blockX, blockY);
            }
            top = PlayUI::Inst()->GetPlayFrame()->y;
        }
    }
    _Win = false;
}

