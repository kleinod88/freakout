/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "StateManager.h"
#include "SplashState.h"
#include "PlayState.h"
#include "GameOverState.h"
#include "PauseState.h"
#include <MainMenu.h>
#include <ScoreMenu.h>
#include "Game.h"


StateManager* StateManager::_Instance = nullptr;

StateManager::StateManager()
{
	_CurrentState = 0;
	for (int i = 0; i < MAXSTATES; i++)
	{
		_States[i] = nullptr;
	}
}

GameState* StateManager::CurrentState()
{
    if (_States[_CurrentState] == nullptr)
    {
        SDL_Log("GameState >%d< nicht geladen aber angefordert\n", _CurrentState);
    }
    return _States[_CurrentState];
}

StateManager* StateManager::Inst()
{
	if (_Instance == nullptr)
		_Instance = new StateManager;
	return _Instance;
}

bool StateManager::AddState(GameStates state)
{
	if (_States[state] != nullptr)
	{
        SDL_Log("Zu ladener State >%d< ist schon geladen\n", state);
		return false;
	}
    
    switch (state)
    {
    case SPLASHSTATE:
        _States[state] = new SplashState();
        _States[state]->OnEnter();
        //Das nur, damit der Splash beim Start gleich an
        _States[state]->Render();
        break;
    case PLAYSTATE:
        _States[state] = new PlayState();
        _States[state]->OnEnter();
        break;
    case PAUSESTATE:
        _States[state] = new PauseState();
        _States[state]->OnEnter();
        break;
    case MAINMENUSTATE:
            _States[state] = new MainMenu();
            _States[state]->OnEnter();
            break;
    case SCORESTATE:
        _States[state] = new ScoreMenu();
        _States[state]->OnEnter();
        break;
        /*    //Lade den MenuState
            //TODO solange kein menustate da ist wird es der Playstate
            _States[PAUSESTATE] = new SplashState();
            _States[PAUSESTATE]->OnEnter(_Renderer);
            StateManager::Inst()->AddState(tmpState, PAUSSTATE);

            //Lade den MenuState
            //TODO solange kein menustate da ist wird es der Playstate
            _States[SCORESTATE] = new SplashState();
            _States[SCORESTATE]->OnEnter(_Renderer);
            StateManager::Inst()->AddState(tmpState, SPLASHSTATE);
            */
            //Lade den MenuState
            //TODO solange kein menustate da ist wird es der Playstate
    case GAMEOVERSTATE:
        _States[state] = new GameOverState();
        _States[state]->OnEnter();
        break;
    default:
        return false;
        break;
    }

	return true;
}

bool StateManager::ChangeState(GameStates state)
{
	if (_States[state] == nullptr)
	{
        SDL_Log("Der Angeforderte State >%d< ist nicht geladen\n", state);
		return false;
	}
	_CurrentState = state;

    //FIXME: ohne das delay ist es nicht wirklich m�glich in z.b Pause zu wechseln, da die states hin und her springen
    SDL_Delay(202);
	return true;
}
