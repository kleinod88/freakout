/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include <MainMenu.h>
#include <Button.h>
#include <Input.h>
#include <TextureManager.h>
#include <StateManager.h>



class PlayButton : public Button {
public:
    PlayButton(Widget* parent, std::string buttonText, std::string themeName = "BUTTON")
        :Button(parent, buttonText, themeName)
    {}
    void Update() override
    {
        if (InputHandler::Instance()->A())
            StateManager::Inst()->ChangeState(PLAYSTATE);
    }
};

class OptionButton : public Button {
public:
    OptionButton(Widget* parent, std::string buttonText, std::string themeName = "BUTTON")
        :Button(parent, buttonText, themeName)
    {}
    void Update() override
    {
        if (InputHandler::Instance()->A())
            StateManager::Inst()->ChangeState(SCORESTATE);
    }
};

class ExitButton : public Button {
public:
    ExitButton(Widget* parent, std::string buttonText, std::string themeName = "BUTTON")
        :Button(parent, buttonText, themeName)
    {}
    void Update() override
    {///muss noch besser werden
        if (InputHandler::Instance()->A())
            SDL_Quit();
    }
};

MainMenu::MainMenu()
{
}

void MainMenu::OnExit()
{
}

void MainMenu::Render()
{
    Box->Render();
}

bool MainMenu::Update()
{

    Box->Update();
    return true;
}

void MainMenu::OnEnter()
{
    //rect1 = new SDL_FRect;
    int w, h;
    SDL_GetRendererOutputSize(TextureManager::Inst()->GetRenderer(), &w, &h);

    rect1.x = w * 0.3;
    rect1.y = h * 0.2;
    rect1.w = w * 0.3;
    rect1.h = h * 0.8;

    Box = new HBox(nullptr, &rect1);

    Box->AddChild(new PlayButton(Box, "Spielen"));
    Box->AddChild(new OptionButton(Box, "Highscore"));
    Box->AddChild(new ExitButton(Box, "Beenden"));
}


