/*
 * Copyright (C) 2021  Frank Kartheuser <frank.kartheuser1988@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include <GameOverState.h>
#include <Input.h>
#include <TextureManager.h>

GameOverState::GameOverState()
{

}

void GameOverState::OnExit()
{
}

void GameOverState::Render()
{

    i++;
    if (i < 30)
    {
        TextureManager::Inst()->DrawFS("GAMEOVER");
    }
    else if (i <= 60)
    {
        TextureManager::Inst()->DrawFS("GAMEOVER2");
    }
    if (i == 60)
        i = 0;

}

bool GameOverState::Update()
{
    
    if (InputHandler::Instance()->A())
    {
        return false;
    }
    return true;
}

void GameOverState::OnEnter()
{

    i = 0;
    
}

